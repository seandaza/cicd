
from flask import Flask
import chromedriver_binary  # Adds chromedriver binary to path
import os
import sys
import time
import smtplib
import pandas as pd
from base64 import encode
from datetime import datetime
from selenium import webdriver
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from selenium.webdriver.common.by import By
from email.mime.multipart import MIMEMultipart
from selenium.webdriver.chrome.options import Options

app = Flask(__name__)

# The following options are required to make headless Chrome
# work in a Docker container
chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument("--headless")
chrome_options.add_argument("--disable-gpu")
chrome_options.add_argument("window-size=1024,768")
chrome_options.add_argument("--no-sandbox")

# Initialize a new browser
browser = webdriver.Chrome(chrome_options=chrome_options)


@app.route("/")
def hello_bot():

    now = datetime.now() 
    year_month_day_hour_minute = now.strftime("%Y-%m-%d %H-%M")
    
    driver = webdriver.Chrome(chrome_options=chrome_options)

    #set url feed for login
    url = 'https://network.americanexpress.com/globalnetwork/v4/sign-in/'

    payload={
        "Email": 'anastasiareyes1987',
        "Password": 'Casa1234??'
    }


    #Navigate to the page
    driver.get(url)
    driver.maximize_window()



    user_ID = driver.find_element("xpath", "//*[@id='userid']")
    user_ID.send_keys(payload['Email'])

    password = driver.find_element("xpath", "//*[@id='password']")
    password.send_keys(payload['Password'])
    time.sleep(2)

    driver.execute_script("window.scrollTo(0, 500);")
    time.sleep(1)

                                                
    signin_button = driver.find_element("xpath", '//*[@id="submit"]')
    #time.sleep(2
    signin_button.click()
    time.sleep(1)

    #Go to FraudNet Reports
    #Link only for 'Active' status reports
    new_url = "https://gnsfraudnet.americanexpress.com/fraudnet/#/ior/search"
    time.sleep(2)



    driver.get(new_url)
    time.sleep(2)
    search_button = driver.find_element("xpath", "//button[@class='button primary pull-right margin-right']")
    search_button.click()

    time.sleep(2)

    #scroll top down
    driver.execute_script("window.scrollTo(0, 500);")
    #driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

    #xpaths of the reports
    tbody = driver.find_element("xpath", '//*[@id="responsiveWrapper_sub"]/div[3]/div[2]/div/div/div[2]/div[2]/div[2]/table/tbody')

    #identify the number of rows or reports
    rows = tbody.find_elements(By.TAG_NAME, "tr")
    print("numero de Reportes: ",len(rows))

    #Create a dataframe to store the data
    total_rows = []
    for j in range(1,len(rows) + 1):
        row = []
        for i in range(1,8):
            celda = driver.find_element("xpath",'//*[@id="responsiveWrapper_sub"]/div[3]/div[2]/div/div/div[2]/div[2]/div[2]/table/tbody/tr['+str(j)+']/td['+str(i)+']').get_attribute('innerHTML').replace("\n",'').strip()
            row.append(celda)
        total_rows.append(row)
    print(total_rows)

    #Declare the dataframe
    df = pd.DataFrame(total_rows)
    print(df)


    try:
        #read the last version of the report
        last_status = pd.read_csv('last_view.csv')
    except:
        #Create and read a reference to the current working directory if don´t exist at first time
        df.to_csv('last_view.csv', index=False)
        last_status = pd.read_csv('last_view.csv')
        
    #Validate new reports:
    new_reports = []
    for i in range(len(rows)):
        if total_rows[i][3][0:10] != year_month_day_hour_minute[0:10]:
            print("No new reports")
            pass
        else:
            new_reports.append(total_rows[i])
    print("New reports: ",new_reports)
    if len(new_reports) > 0:
            print("there are new reports")
            #declare and build the dataframe
            df2 = pd.DataFrame(new_reports)
            #save the dataframe to a csv file with the last reports
            df2.to_csv('FraudNet_new_report_' + year_month_day_hour_minute + '.csv', index=False)

            #Send email with the new reports
            #screen shot and save in local
            driver.save_screenshot('new_report.png')

            #send email
            # create message object instance
            recipients = ["jhand@keoworld.com", 'seandaza44@gmail.com']
            for elm in recipients:
                msg = MIMEMultipart()
                # setup the parameters of the message
                password = "kvjxjjghzpqfdpcd"
                msg['From'] = "jhand@keoworld.com"
                msg['Subject'] = "FraudnetBot Alert: New Report"
                msg['To'] = f"{elm}"
                
                # attach image and text to message body
                for i in range(len(new_reports)):
                    msg.attach(MIMEText('new report found: ' + str(new_reports[i]) + '\n'))
                msg.attach(MIMEImage(open('new_report.png', 'rb').read()))
                
                # create server
                server = smtplib.SMTP('smtp.outlook.com: 587')
                server.starttls()
                
                # Login Credentials for sending the mail
                server.login(msg['From'], password)
                
                # send the message via the server.
                server.sendmail(msg['From'], msg['To'], msg.as_string())
                
                server.quit()
                print("Email sent successfully")
    else:
        print("No new reports")
        pass

    #Validate new status:

    #Identify the status of the last report
    new_status = []
    for i in range(len(last_status)):
        if last_status['5'][i] == total_rows[i][5]:
            print("No new status")
            pass
        else:
            print("New status: ",total_rows[i])
            new_status.append(total_rows[i])
            
    if len(new_status) > 0:
        print("there are new status")
        #declare and build the dataframe for the new status
        df1 = pd.DataFrame(new_status)
        print(df1)
        print("There are some new status")
        print("Please check the data")  
        #save the dataframe to a csv file with the last new status
        df1.to_csv('Fraudnet_new_status_' + year_month_day_hour_minute + '.csv',index=False)

        #save the new status y/o new reports in 'last_view.csv'
        df.to_csv('last_view.csv', index=False)

        #Report the new status in email:

        #screen shot and save in local
        driver.save_screenshot('new_status.png')
        # create message object instance

        recipients = ["jhand@keoworld.com", 'seandaza44@gmail.com']
        for elm in recipients:
            msg = MIMEMultipart()
            # setup the parameters of the message
            password = "kvjxjjghzpqfdpcd"
            msg['From'] = "jhand@keoworld.com"
            msg['Subject'] = "FraudnetBot Alert: New Status"
            msg['To'] = f"{elm}"
            
            # attach image and text to message body
            for i in range(len(new_status)):
                msg.attach(MIMEText('new status found: ' + str(new_status[i]) + '\n'))
            msg.attach(MIMEImage(open('new_status.png', 'rb').read()))
            
            # create server
            server = smtplib.SMTP('smtp.outlook.com: 587')
            server.starttls()
            
            # Login Credentials for sending the mail
            server.login(msg['From'], password)
            
            # send the message via the server.
            server.sendmail(msg['From'], msg['To'], msg.as_string())
            
            server.quit()
            print("Email sent successfully")
    else:
        print("No new status")
        pass
    driver.quit()
    return "Done"